# Projektarbeit
## Mitarbeiterverwaltung – Personnel Management App
cimdata Java III (Erweiterungskurs) – Dozent/Projektleiter: Michael Schirmer – Projektarbeit: Ben Borchers – 03.2021

### Features
- Konzentration auf Urlaubsanträge (Leave Requests)
- UI: JSF und Primefaces
- DB: JPA Hibernate DB-Anbindung
- Server: Spring Boot


#### Notice
Ich habe mit MacOS gearbeitet und MAMP als Webserver genutzt.  
Entsprechende Anpassungen in der application.properties wie folgt:  
- spring.datasource.url=jdbc:mysql://127.0.0.1:3306/java3_pa?serverTimezone=UTC&createDatabaseIfNotExist=true  
- spring.datasource.username=root  
- spring.datasource.password=root  