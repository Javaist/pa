package cimdata.java3.pa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LeaveRequest;

@Repository
public interface EmployeeDAO extends CrudRepository<Employee, Integer>{

	@Override
	List<Employee> findAll();
	
	
	Employee findEmployeeByLoginUsername(String username);
	
	Employee findEmployeeByEmployeeId(String employeeId);
	
		
}
