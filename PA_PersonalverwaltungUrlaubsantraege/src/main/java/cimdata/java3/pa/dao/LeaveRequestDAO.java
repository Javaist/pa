package cimdata.java3.pa.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cimdata.java3.pa.model.EmployeeData;
import cimdata.java3.pa.model.LeaveRequest;

@Repository
public interface LeaveRequestDAO extends CrudRepository<LeaveRequest, Integer> {
	
	@Override
	List<LeaveRequest> findAll();
	
	List<LeaveRequest> findAllByEmployeeData(EmployeeData employeeData);
	
	Optional<LeaveRequest> findById(Long id);
	
}
