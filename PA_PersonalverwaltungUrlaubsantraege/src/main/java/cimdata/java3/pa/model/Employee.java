package cimdata.java3.pa.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
public class Employee implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // für DB
	@Column(length = 10, unique = true, updatable = false )
	private String employeeId;
	@Column(length = 20 )
	private String firstname;
	@Column(length = 30 )
	private String lastname;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 10 )
	private UserAuthorisation authorisation;  // Benutzerrechte
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "employee_data_id") )
	private EmployeeData employeeData;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "login_id") )
	private LoginDetails login;
	

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String employeeId, String firstname, String lastname, UserAuthorisation authorisation,
			EmployeeData employeeData, LoginDetails login) {
		this.employeeId = employeeId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.authorisation = authorisation;
		this.employeeData = employeeData;
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public UserAuthorisation getAuthorisation() {
		return authorisation;
	}

	public void setAuthorisation(UserAuthorisation authorisation) {
		this.authorisation = authorisation;
	}

	public EmployeeData getEmployeeData() {
		return employeeData;
	}

	public void setEmployeeData(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public LoginDetails getLogin() {
		return login;
	}

	public void setLogin(LoginDetails login) {
		this.login = login;
	}

	@Override
	public boolean equals(final Object other) {
		 if (this == other) {
	            return true;
	        }
		if (!(other instanceof Employee)) {
			return false;
		}
		Employee castOther = (Employee) other;
		return new EqualsBuilder().append(employeeId, castOther.employeeId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(employeeId).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("employeeId", employeeId)
				.append("firstname", firstname).append("lastname", lastname).append("authorisation", authorisation)
				.append("employeeData", "...").append("login-Username", login.getUsername()).toString();
	}

}
