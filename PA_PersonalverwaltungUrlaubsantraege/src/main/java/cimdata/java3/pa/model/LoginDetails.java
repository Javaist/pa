package cimdata.java3.pa.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.EqualsExclude;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.HashCodeExclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringExclude;

@Entity
@Table(name = "logins")
public class LoginDetails implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 20, unique = true, nullable = false )
	private String username;
	private String password;
	
	private LocalDate lastLogin;       // last successful login
	private int incorrectLoginCount;   // after 3 fails account is blocked; if login succeed on third or less attempt, counter reset
	private boolean blocked;
	
	@OneToOne(mappedBy = "login", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@ToStringExclude @HashCodeExclude @EqualsExclude
	private Employee employee;

	
	public LoginDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginDetails(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getLastLogin() {
		return lastLogin;
	}

	public void setLastLogins(LocalDate lastLogin) {
		this.lastLogin = lastLogin;
	}

	public int getIncorrectLoginCount() {
		return incorrectLoginCount;
	}

	public void setIncorrectLoginCount(int incorrectLoginCount) {
		this.incorrectLoginCount = incorrectLoginCount;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof LoginDetails)) {
			return false;
		}
		LoginDetails castOther = (LoginDetails) other;
		return new EqualsBuilder().append(username, castOther.username)
				.append(employee, castOther.employee)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(username).append(employee).toHashCode();
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this);
		builder.append("username", username);
		builder.append("employee", employee);
		builder.append("password", password);
		builder.append("lastLogin", lastLogin);
		builder.append("incorrectLoginCount", incorrectLoginCount);
		builder.append("blocked", blocked);
		return builder.toString();
	}


}
