package cimdata.java3.pa.model;

public enum UserAuthorisation {

	USER, SUPERVISOR, ADMIN
}
