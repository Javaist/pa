package cimdata.java3.pa.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Address implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToOne(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EmployeeData employeeData;
	
	@Column(length = 50 )
	private String street;
	@Column(length = 10 )
	private String streetNumber;
	@Column(length = 5 )
	private int postcode;  // PLZ
	@Column(length = 30 )
	private String city;
	
	public Address() {
		// TODO Auto-generated constructor stub
	}

	public Address(String street, String streetNumber, int postcode, String city) {
		this.street = street;
		this.streetNumber = streetNumber;
		this.postcode = postcode;
		this.city = city;
	}

	public EmployeeData getEmployeeData() {
		return employeeData;
	}

	public void setEmployeeData(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Address)) {
			return false;
		}
		Address castOther = (Address) other;
		return new EqualsBuilder()
				.append(street, castOther.street).append(streetNumber, castOther.streetNumber)
				.append(postcode, castOther.postcode).append(city, castOther.city).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(street).append(streetNumber)
				.append(postcode).append(city).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("street", street)
				.append("streetNumber", streetNumber).append("postcode", postcode).append("city", city).toString();
	}
	
}
