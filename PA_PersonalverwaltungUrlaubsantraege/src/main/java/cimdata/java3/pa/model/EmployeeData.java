package cimdata.java3.pa.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "employee_data")
public class EmployeeData implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne(mappedBy = "employeeData", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Employee employee;

	@Column(updatable = false)
	private LocalDate dob;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "address_id"))
	private Address address;
	@Column(length = 30)
	private String email;

	private LocalDate dayOfHire;
	private Double salary; // Gehalt/Lohn

	private boolean temporaryEmployment;
	private LocalDate temporaryEmploymentTo;

	private int daysOfIllness;

	private int leaveEntitlementPerAnnum;
	private int takenLeave;

	@OneToMany(mappedBy = "employeeData", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<LeaveRequest> leaveRequestList = new ArrayList<>();

	public EmployeeData() {
		// TODO Auto-generated constructor stub
	}

	public EmployeeData(LocalDate dob, Address address, String email, LocalDate dayOfHire, Double salary,
			boolean temporaryEmployment, LocalDate temporaryEmploymentTo, int daysOfIllness,
			int leaveEntitlementPerAnnum, int remainingLeave) {
		this.dob = dob;
		this.address = address;
		this.email = email;
		this.dayOfHire = dayOfHire;
		this.salary = salary;
		this.temporaryEmployment = temporaryEmployment;
		this.temporaryEmploymentTo = temporaryEmploymentTo;
		this.daysOfIllness = daysOfIllness;
		this.leaveEntitlementPerAnnum = leaveEntitlementPerAnnum;
		this.takenLeave = remainingLeave;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDayOfHire() {
		return dayOfHire;
	}

	public void setDayOfHire(LocalDate dayOfHire) {
		this.dayOfHire = dayOfHire;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public boolean isTemporaryEmployment() {
		return temporaryEmployment;
	}

	public void setTemporaryEmployment(boolean temporaryEmployment) {
		this.temporaryEmployment = temporaryEmployment;
	}

	public LocalDate getTemporaryEmploymentTo() {
		return temporaryEmploymentTo;
	}

	public void setTemporaryEmploymentTo(LocalDate temporaryEmploymentTo) {
		this.temporaryEmploymentTo = temporaryEmploymentTo;
	}

	public int getDaysOfIllness() {
		return daysOfIllness;
	}

	public void setDaysOfIllness(int daysOfIllness) {
		this.daysOfIllness = daysOfIllness;
	}

	public int getLeaveEntitlementPerAnnum() {
		return leaveEntitlementPerAnnum;
	}

	public void setLeaveEntitlementPerAnnum(int leaveEntitlementPerAnnum) {
		this.leaveEntitlementPerAnnum = leaveEntitlementPerAnnum;
	}

	public int getTakenLeave() {
		return takenLeave;
	}

	public void setTakenLeave(int remainingLeave) {
		this.takenLeave = remainingLeave;
	}

	public List<LeaveRequest> getLeaveRequestList() {
		return leaveRequestList;
	}

	public void setLeaveRequestList(List<LeaveRequest> leaveRequestList) {
		this.leaveRequestList = leaveRequestList;
	}

	public LeaveRequest addLeaveRequest(LeaveRequest lr) {
		lr.setEmployeeData(this);
		boolean added = this.leaveRequestList.add(lr);
		if (added)
			return lr;

		return null;
	}

	public LeaveRequest removeLeaveRequest(LeaveRequest lr) {
		lr.setEmployeeData(null);
		boolean removed = this.leaveRequestList.remove(lr);
		if(removed) 
			return lr;
		return null;
	}

	@Override
	public boolean equals(final Object other) {
		if(this == other ) {
			return true;
		}
		if (!(other instanceof EmployeeData)) {
			return false;
		}
		EmployeeData castOther = (EmployeeData) other;
		return new EqualsBuilder().append(dob, castOther.dob)
				.append(employee, castOther.employee)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(dob).append(employee).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("employee", employee).append("dob", dob).append("address", address)
				.append("email", email).append("dayOfHire", dayOfHire).append("salary", salary)
				.append("temporaryEmployment", temporaryEmployment)
				.append("temporaryEmploymentTo", temporaryEmploymentTo).append("daysOfIllness", daysOfIllness)
				.append("leaveEntitlementPerAnnum", leaveEntitlementPerAnnum).append("takenLeave", takenLeave)
				.append("leaveRequestList", leaveRequestList).toString();
	}

}