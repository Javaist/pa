package cimdata.java3.pa.model;

public enum RequestState {
	
	UNCONFIRMED, APPROVED, REJECTED 
}
