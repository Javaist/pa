package cimdata.java3.pa.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;


@Entity
@Table(name = "leave_requests")
public class LeaveRequest implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "employee_data_id"))
	private EmployeeData employeeData;  // Antragssteller
	
	@Column(updatable = false, nullable = false)
	private LocalDateTime submitDate;
	private LocalDate leaveFrom;
	private LocalDate leaveTo;
	private int leaveDays;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 11)
	private RequestState requestState = RequestState.UNCONFIRMED;
	@Column(columnDefinition = "Text")
	private String statement;
	
	public LeaveRequest() {
		// TODO Auto-generated constructor stub
	}

	public LeaveRequest(LocalDate from, LocalDate till) {
		this.leaveFrom = from;
		this.leaveTo = till;
		this.submitDate = LocalDateTime.now();
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public EmployeeData getEmployeeData() {
		return employeeData;
	}

	public void setEmployeeData(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public LocalDateTime getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(LocalDateTime submitDate) {
		this.submitDate = submitDate;
	}

	public LocalDate getLeaveFrom() {
		return leaveFrom;
	}

	public void setLeaveFrom(LocalDate from) {
		this.leaveFrom = from;
	}

	public LocalDate getLeaveTo() {
		return leaveTo;
	}

	public void setLeaveTo(LocalDate till) {
		this.leaveTo = till;
	}

	public int getLeaveDays() {
		return leaveDays;
	}
	
	public void setLeaveDays(int leaveDays) {
		this.leaveDays = leaveDays;
	}

	public RequestState getRequestState() {
		return requestState;
	}

	public void setRequestState(RequestState approved) {
		this.requestState = approved;
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
        if (!(o instanceof LeaveRequest )) return false;
        return id == (((LeaveRequest) o).getId());
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("employeeData", employeeData)
				.append("leaveFrom", leaveFrom).append("leaveTo", leaveTo).append("leaveDays", leaveDays)
				.append("requestApproved", requestState).append("statement", statement).toString();
	}

}