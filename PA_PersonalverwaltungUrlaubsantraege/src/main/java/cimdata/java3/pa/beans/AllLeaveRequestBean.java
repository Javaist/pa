package cimdata.java3.pa.beans;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;

import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.model.RequestState;
import cimdata.java3.pa.service.EmployeeService;
import cimdata.java3.pa.service.LeaveRequestService;
import cimdata.java3.pa.util.CalculateLeaveDays;
import cimdata.java3.pa.util.OnScreenMessage;

@ViewScoped
@Named
public class AllLeaveRequestBean {

	@Autowired
	private LoginBean loginBean;

	@Autowired
	private EmployeeBean empBean;

	@Autowired
	private LeaveRequestService lrService;

	@Autowired
	private EmployeeService empService;

	private Employee currentEmp = new Employee();

	private List<LeaveRequest> allLeaveRequests = new ArrayList<LeaveRequest>();
	private List<LeaveRequest> filteredRequests;
	private List<FilterMeta> filterBy = new ArrayList<>();
	private String unconfirmedLeaveRequests = "0";

	private LeaveRequest currentLeaveRequest = new LeaveRequest();
	private boolean showNewLeaveRequest = false;

	private LeaveRequest selectedLeaveRequest;

	private List<LocalDate> range;
	private int currentleaveDays = 0;

	private List<Integer> invalidDays = new ArrayList<>();
	private LocalDate minDate = LocalDate.now();

	@PostConstruct
	public void init() {
		System.out.println("All_LeaveRequestBean init ...");
		refresh();

		// Liste mit Wochentagen, die im DatePicker nicht angewählter werden können
		// sollen
		invalidDays.add(6); // Samstag
		invalidDays.add(0); // Sonntag

		filterBy.add(FilterMeta.builder().field("requestState").filterValue(RequestState.UNCONFIRMED)
				.matchMode(MatchMode.EQUALS).build());

	}

	public void refresh() {
		allLeaveRequests = lrService.getAll();

		int tempCounter = 0;
		for (LeaveRequest leaveRequest : allLeaveRequests) {
			if (leaveRequest.getRequestState().equals(RequestState.UNCONFIRMED)) {
				tempCounter++;
				if (tempCounter > 10) {
					unconfirmedLeaveRequests = "10+";
					return;
				} else {
					unconfirmedLeaveRequests = String.valueOf(tempCounter);
				}
			}
		}

	}

	public void onDateSelect() {
		currentLeaveRequest.setLeaveFrom(range.get(0));
		currentLeaveRequest.setLeaveTo(range.get(1));

		currentleaveDays = CalculateLeaveDays.calculate(currentLeaveRequest.getLeaveFrom(),
				currentLeaveRequest.getLeaveTo());
		currentLeaveRequest.setLeaveDays(currentleaveDays);
	}

	public void approveLeaveRequest() {
		System.out.println("approveLeaveRequest");
		LeaveRequest updated = lrService.updateLeaveRequest(selectedLeaveRequest.getId(), RequestState.APPROVED);
		if(updated != null) {
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_INFO, "Urlaubsantrag genehmigt", "");
			refresh();
		}
	}

	public void rejectedLeaveRequest() {
		System.out.println("rejectedLeaveRequest");
			refresh();
	}
	
	public void saveLeaveRequest() {
		currentLeaveRequest.setEmployeeData(currentEmp.getEmployeeData());
		currentLeaveRequest.setSubmitDate(LocalDateTime.now());

		if (currentLeaveRequest.getLeaveFrom() == null || currentLeaveRequest.getLeaveTo() == null) {
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_WARN, "Zeitraum ist leer",
					"Bitte wählen Sie einen Zeitraum aus.");
		} else {
			LeaveRequest saved = lrService.saveLeaveRequest(currentLeaveRequest);
			if (saved != null) {
				currentLeaveRequest = new LeaveRequest();
				range = new ArrayList<LocalDate>();
				currentleaveDays = 0;
				empBean.refresh();
				OnScreenMessage.addMessage(FacesMessage.SEVERITY_INFO, "Erfolgreich Verschickt",
						"Ihr Urlaubsantrag wurde gespeichert und zur Prüfung weitergeleitet.");
			}
		}
	}

	public int previewRemainingLeave() {
		int remainingLeave = currentEmp.getEmployeeData().getLeaveEntitlementPerAnnum()
				- currentEmp.getEmployeeData().getTakenLeave();
		return remainingLeave - currentleaveDays;
	}

	public int previewRemainingLeaveUnconfirmed() {
		return empBean.remainingLeaveDaysUnconfirmed() - currentLeaveRequest.getLeaveDays();
	}

	public void switchShowNewLeaveRequest() {
		showNewLeaveRequest = !showNewLeaveRequest;
	}

	public List<String> getEmployeeNames() {
		List<Employee> Employees = empService.getAll();
		ArrayList<String> lastnames = new ArrayList<String>();
		for (Employee employee : Employees) {
			lastnames.add(
					employee.getFirstname() + " " + employee.getLastname() + " (" + employee.getEmployeeId() + ")");
		}

		return lastnames;
	}

	public RequestState[] getRequestStates() {
		return RequestState.values();
	}

	public List<LeaveRequest> getAllLeaveRequests() {
		return allLeaveRequests;
	}

	public String getUnconfirmedLeaveRequests() {
		return unconfirmedLeaveRequests;
	}

	public void setUnconfirmedLeaveRequests(String unconfirmedLeaveRequests) {
		this.unconfirmedLeaveRequests = unconfirmedLeaveRequests;
	}

	public void setAllLeaveRequests(List<LeaveRequest> allLeaveRequests) {
		this.allLeaveRequests = allLeaveRequests;
	}

	public List<LeaveRequest> getFilteredRequests() {
		return filteredRequests;
	}

	public void setFilteredRequests(List<LeaveRequest> filteredRequests) {
		this.filteredRequests = filteredRequests;
	}

	public List<FilterMeta> getFilterBy() {
		return filterBy;
	}

	public void setFilterBy(List<FilterMeta> filterBy) {
		this.filterBy = filterBy;
	}

	public boolean isShowNewLeaveRequest() {
		return showNewLeaveRequest;
	}

	public void setShowNewLeaveRequest(boolean showNewLeaveRequest) {
		this.showNewLeaveRequest = showNewLeaveRequest;
	}

	public LeaveRequest getSelectedLeaveRequest() {
		return selectedLeaveRequest;
	}

	public void setSelectedLeaveRequest(LeaveRequest selectedLeaveRequest) {
		this.selectedLeaveRequest = selectedLeaveRequest;
	}

	public LeaveRequest getCurrentLeaveRequest() {
		return currentLeaveRequest;
	}

	public void setCurrentLeaveRequest(LeaveRequest currentLeaveRequest) {
		this.currentLeaveRequest = currentLeaveRequest;
	}

	public List<LocalDate> getRange() {
		return range;
	}

	public void setRange(List<LocalDate> range) {
		this.range = range;
	}

	public int getCurrentleaveDays() {
		return currentleaveDays;
	}

	public void setCurrentleaveDays(int currentleaveDays) {
		this.currentleaveDays = currentleaveDays;
	}

	public List<Integer> getInvalidDays() {
		return invalidDays;
	}

	public void setInvalidDays(List<Integer> invalidDays) {
		this.invalidDays = invalidDays;
	}

	public LocalDate getMinDate() {
		return minDate;
	}

	public void setMinDate(LocalDate minDate) {
		this.minDate = minDate;
	}

}
