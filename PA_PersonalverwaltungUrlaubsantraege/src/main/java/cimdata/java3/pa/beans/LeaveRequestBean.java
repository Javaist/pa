package cimdata.java3.pa.beans;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.model.UserAuthorisation;
import cimdata.java3.pa.service.LeaveRequestService;
import cimdata.java3.pa.util.CalculateLeaveDays;
import cimdata.java3.pa.util.OnScreenMessage;

@ViewScoped
@Named
public class LeaveRequestBean {

	@Autowired
	private LoginBean loginBean;
	
	@Autowired
	private EmployeeBean empBean;
	
	@Autowired
	private AllLeaveRequestBean AllLRBean;

	@Autowired
	private LeaveRequestService lrService;

	private Employee currentEmp = new Employee();

	private LeaveRequest currentLeaveRequest = new LeaveRequest();
	private boolean showNewLeaveRequest = false;

	private List<LocalDate> range;
	private int currentleaveDays = 0;

	private List<Integer> invalidDays = new ArrayList<>();
	private LocalDate minDate = LocalDate.now().plusDays(14); // Urlaub soll mit mindestens zwei Wochen Vorlauf genommen werden; also frühstens ab heute in 14 Tagen 

	@PostConstruct
	public void init() {
		System.out.println("LeaveRequestBean init ...");
		currentEmp = loginBean.getCurrentEmp();
		System.out.println("LR_Bean_curEmp:" + currentEmp.getFirstname());
		
		// Liste mit Wochentagen, die im DatePicker nicht angewählter werden können
		// sollen
		invalidDays.add(6); // Samstag
		invalidDays.add(0); // Sonntag
	}
	
	public void onDateSelect() {
//		System.out.println("on");
//
//		System.out.println("von: " + range.get(0));
//		System.out.println("bis: " + range.get(1));
		currentLeaveRequest.setLeaveFrom(range.get(0));
		currentLeaveRequest.setLeaveTo(range.get(1));
		
		currentleaveDays = CalculateLeaveDays.calculate(currentLeaveRequest.getLeaveFrom(),
				currentLeaveRequest.getLeaveTo());
		currentLeaveRequest.setLeaveDays(currentleaveDays);
	}
	
	public void saveLeaveRequest() {
		currentLeaveRequest.setEmployeeData(currentEmp.getEmployeeData());
		currentLeaveRequest.setSubmitDate(LocalDateTime.now());

		if (currentLeaveRequest.getLeaveFrom() == null || currentLeaveRequest.getLeaveTo() == null) {
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_WARN, "Zeitraum ist leer", "Bitte wählen Sie einen Zeitraum aus.");
		} else {
			LeaveRequest saved = lrService.saveLeaveRequest(currentLeaveRequest);
			if (saved != null) {
				currentLeaveRequest = new LeaveRequest();
				range = new ArrayList<LocalDate>();
				currentleaveDays = 0;
				empBean.refresh();
				if(currentEmp.getAuthorisation().equals(UserAuthorisation.SUPERVISOR)) {
					AllLRBean.refresh();
				}
				OnScreenMessage.addMessage(FacesMessage.SEVERITY_INFO, "Erfolgreich Verschickt",
						"Ihr Urlaubsantrag wurde gespeichert und zur Prüfung weitergeleitet.");
			}
		}
	}

	public int previewRemainingLeave() {
		int remainingLeave = currentEmp.getEmployeeData().getLeaveEntitlementPerAnnum()
				- currentEmp.getEmployeeData().getTakenLeave();
		return remainingLeave - currentleaveDays;
	}

	public int previewRemainingLeaveUnconfirmed() {
		return empBean.remainingLeaveDaysUnconfirmed() - currentLeaveRequest.getLeaveDays();
	}

	public void switchShowNewLeaveRequest() {
		showNewLeaveRequest = !showNewLeaveRequest;
	}


	public boolean isShowNewLeaveRequest() {
		return showNewLeaveRequest;
	}

	public void setShowNewLeaveRequest(boolean showNewLeaveRequest) {
		this.showNewLeaveRequest = showNewLeaveRequest;
	}

	public LeaveRequest getCurrentLeaveRequest() {
		return currentLeaveRequest;
	}

	public void setCurrentLeaveRequest(LeaveRequest currentLeaveRequest) {
		this.currentLeaveRequest = currentLeaveRequest;
	}

	public List<LocalDate> getRange() {
		return range;
	}

	public void setRange(List<LocalDate> range) {
		this.range = range;
	}

	public int getCurrentleaveDays() {
		return currentleaveDays;
	}

	public void setCurrentleaveDays(int currentleaveDays) {
		this.currentleaveDays = currentleaveDays;
	}

	public List<Integer> getInvalidDays() {
		return invalidDays;
	}

	public void setInvalidDays(List<Integer> invalidDays) {
		this.invalidDays = invalidDays;
	}

	public LocalDate getMinDate() {
		return minDate;
	}

	public void setMinDate(LocalDate minDate) {
		this.minDate = minDate;
	}

	public Employee getCurrentEmp() {
		return currentEmp;
	}

	public void setCurrentEmp(Employee currentEmp) {
		this.currentEmp = currentEmp;
	}

}
