package cimdata.java3.pa.beans;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named
public class SubpageBean {
	
	private String subUrl = "/subpages/hallo.xhtml";

	
	
	public String getSubUrl() {
		return subUrl;
	}

	public void setSubUrl(String subUrl) {
		this.subUrl = subUrl;
	}
	
}
