package cimdata.java3.pa.beans;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LoginDetails;
import cimdata.java3.pa.service.EmployeeService;
import cimdata.java3.pa.util.OnScreenMessage;

@SessionScope
@Named
public class LoginBean {

	Logger log = LoggerFactory.getLogger(LoginBean.class);
	
	@Autowired
	private EmployeeService service;
	
	private Employee currentEmp = new Employee();
	
	private LoginDetails currentLoggingInUser = new LoginDetails();
	
	@PostConstruct
	public void init() {
		System.out.println("curUser: " + currentLoggingInUser.getUsername());
		System.out.println("curEmp: " + currentEmp.getFirstname());
	}
	
	public Employee getCurrentEmp() {
		return currentEmp;
	}

	public void setCurrentEmp(Employee currentEmp) {
		this.currentEmp = currentEmp;
	}

	public LoginDetails getCurrentLoggingInUser() {
		return currentLoggingInUser;
	}

	public void setCurrentLoggingInUser(LoginDetails currentInLoggingUser) {
		this.currentLoggingInUser = currentInLoggingUser;
	}


	public String login() {
		
		//  inputfield username is empty
		if(currentLoggingInUser.getUsername().isEmpty()){
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_INFO, "Eingabefelder leer", "Bitte geben Sie Ihren Benutzernamen und Passwort ein.");
			currentLoggingInUser = new LoginDetails();
			return "Start";
		}
		// Login data not exists
		if(service.findEmployeeByUsername(currentLoggingInUser.getUsername()) == null) {
			log.info("Login failed because User is unknown.");
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_INFO, "Zugangsdaten unbekannt", "Bitte prüfen Sie die Anmeldedaten auf korrekte Schreibweise. <br/> Bei Problemen wenden Sie sich bitte an den ADMIN.");
			currentLoggingInUser = new LoginDetails();
			return "Start";
		} 
		// Login Data exists ...
		currentEmp = service.findEmployeeByUsername(currentLoggingInUser.getUsername());
		// ... but User is blocked because to many failed logins
		if(currentEmp.getLogin().isBlocked()) {
			log.info("Login failed because User is blocked.");
			OnScreenMessage.addMessage(FacesMessage.SEVERITY_WARN, "Account gesperrt", "Ihr Zugang wurde gesperrt aufgrund mehrerer fehlgeschlagener Anmeldungen. Bitte wenden Sie sich an den ADMIN.");
			currentLoggingInUser = new LoginDetails();
			return "Start";
		}
		// ... and Login succeeded
		if(currentEmp.getLogin().getPassword().equals(currentLoggingInUser.getPassword())){
			log.info("Login succeeded.");
			currentEmp.getLogin().setIncorrectLoginCount(0);  // Counter reset
			return "MainMenu"; // MainMenu.xhtml
		}
		// ... but password is wrong and login failed
		currentEmp.getLogin().setIncorrectLoginCount(currentEmp.getLogin().getIncorrectLoginCount() + 1);  // Counter ++
		log.warn("Fehlversuche: "+ currentEmp.getLogin().getIncorrectLoginCount());
		// FIXME: Fehlerhafte Logins funktioniert nicht korrekt
		if (currentEmp.getLogin().getIncorrectLoginCount() >= 3) {  
			currentEmp.getLogin().setBlocked(true);
		}
		log.warn("Login failed.");
		OnScreenMessage.addMessage(FacesMessage.SEVERITY_WARN, "Anmeldung fehlgeschlagen!", "Zugansdaten fehlerhaft! Sie haben noch " + (3 - currentEmp.getLogin().getIncorrectLoginCount()) + " Versuche.");
		currentLoggingInUser = new LoginDetails();
		return "Start"; // Start.xhtml
	}
	
	public String logout() {
//		//Session beenden
//		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		currentLoggingInUser = new LoginDetails();
		log.info("Logout!");
		return "Start?faces-redirect=true";
	}
}
