package cimdata.java3.pa.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.service.EmployeeService;
import cimdata.java3.pa.service.LeaveRequestService;
import cimdata.java3.pa.util.CalculateLeaveDays;

@ViewScoped
@Named(value = "empBean")
public class EmployeeBean {

	Logger log = LoggerFactory.getLogger(EmployeeBean.class);
	
	@Autowired
	private LoginBean loginBean;
	
	private Employee currentEmp = new Employee();
			
	private List<LeaveRequest> currentEmpLeaveRequests = new ArrayList<LeaveRequest>();
	
	private LeaveRequest selectedLeaveRequest;
	
	@Autowired
	private EmployeeService empService;
	
	@Autowired
	private LeaveRequestService lrService;
	
	private boolean showLeaveRequests = false;
	
	private int remainingLeaveDaysUncon = 0;
	
	
	@PostConstruct
	public void init() {
		System.out.println("empBean init ...");
		refresh();
		log.info(">> CurrentUser: "+ loginBean.getCurrentEmp().getFirstname() );
	}

	public void refresh() {
		System.out.println("loginBEAN: " + loginBean.getCurrentEmp().getEmployeeId());
		currentEmp = empService.findEmployeeByEmployeeId(loginBean.getCurrentEmp().getEmployeeId());  // TODO User reload requiered
		currentEmpLeaveRequests = currentEmp.getEmployeeData().getLeaveRequestList();
	}
	
	public int remainingLeaveDays() {
		return currentEmp.getEmployeeData().getLeaveEntitlementPerAnnum() - currentEmp.getEmployeeData().getTakenLeave();
	}
	
	public int remainingLeaveDaysUnconfirmed() {
		int unconfirmedLeaveDays = 0;
		for (LeaveRequest leaveRequest : currentEmpLeaveRequests) {
			unconfirmedLeaveDays += CalculateLeaveDays.calculate(leaveRequest.getLeaveFrom(), leaveRequest.getLeaveTo());
		}
		return remainingLeaveDays() - unconfirmedLeaveDays;
	}
	
	// @Transactional  // im JUnitTest war das die lösung, hier tut sich nichts
	public void delete() {
		System.out.println(">>> selectedLeaveRequest.getId(): " + selectedLeaveRequest.getId());
		System.out.println("LISTe:" + currentEmpLeaveRequests.size());

//		boolean removed = currentEmpLeaveRequests.remove(selectedLeaveRequest);
//		if(removed) {
//			FacesMessage msg = new FacesMessage("Gelöscht"," Urlaubsantrag gelöscht!");
//			FacesContext.getCurrentInstance().addMessage(null, msg);
//			refresh();
//		}
		
//		Employee emp = empService.findEmployeeByEmployeeId(selectedLeaveRequest.getEmployeeData().getEmployee().getEmployeeId());
////		
//		boolean removed = emp.getEmployeeData().getLeaveRequestList().remove(selectedLeaveRequest);
//		System.out.println("emp >>> data >>> lr: removed: " + removed);
////	
		
		
		
		boolean deleted = lrService.deleteLeaveRequest(selectedLeaveRequest.getId());
//		System.out.println("deleted? " + deleted);
//		if(deleted) {
//			FacesMessage msg = new FacesMessage("Gelöscht"," Urlaubsantrag gelöscht!");
//			FacesContext.getCurrentInstance().addMessage(null, msg);
//			refresh();
//		}
		
//		LeaveRequest deleted = currentEmp.getEmployeeData().removeLeaveRequest(selectedLeaveRequest);
		if(deleted) {
			FacesMessage msg = new FacesMessage("Gelöscht"," Urlaubsantrag gelöscht!");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			refresh();
		}
		
		
		
		
//		refresh();
		System.out.println("LISTe:" + currentEmpLeaveRequests.size());
		
	}
	
	

	public Employee getCurrentEmp() {
		return currentEmp;
	}

	public void setCurrentEmp(Employee currentEmp) {
		this.currentEmp = currentEmp;
	}

	public List<LeaveRequest> getCurrentEmpLeaveRequests() {
		return currentEmpLeaveRequests;
	}

	public void setCurrentEmpLeaveRequests(List<LeaveRequest> currentEmpLeaveRequests) {
		this.currentEmpLeaveRequests = currentEmpLeaveRequests;
	}

	public LeaveRequest getSelectedLeaveRequest() {
		return selectedLeaveRequest;
	}

	public void setSelectedLeaveRequest(LeaveRequest selectedLeaveRequest) {
		this.selectedLeaveRequest = selectedLeaveRequest;
	}

	public EmployeeService getEmpService() {
		return empService;
	}

	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}

	public boolean isShowLeaveRequests() {
		return showLeaveRequests;
	}

	public void setShowLeaveRequests(boolean showLeaveRequests) {
		this.showLeaveRequests = showLeaveRequests;
	}
	
	public void switchShowLeaveRequests() {
		showLeaveRequests = !showLeaveRequests;
	}

	public int getRemainingLeaveDaysUncon() {
		return remainingLeaveDaysUncon;
	}

	public void setRemainingLeaveDaysUncon(int remainingLeaveDaysUncon) {
		this.remainingLeaveDaysUncon = remainingLeaveDaysUncon;
	}
}
