package cimdata.java3.pa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cimdata.java3.pa.dao.EmployeeDAO;
import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.LeaveRequest;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDAO dao;
	
	
    /**
     * save given Employee
     * @param  Employee emp
     * @return  Employee
     */
	public Employee attachEmployee(Employee emp) {
		return dao.save(emp);
	}
	
	
	public List<Employee> getAll(){
		return dao.findAll();
	}
	
	
	public Employee findEmployeeByUsername(String username) {
		return dao.findEmployeeByLoginUsername(username);
	}
	
	
	public Employee findEmployeeByEmployeeId(String empId) {
		return dao.findEmployeeByEmployeeId(empId);
	}
	
	
	
}
