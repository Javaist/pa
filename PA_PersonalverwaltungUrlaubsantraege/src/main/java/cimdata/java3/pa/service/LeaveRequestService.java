package cimdata.java3.pa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cimdata.java3.pa.dao.LeaveRequestDAO;
import cimdata.java3.pa.model.EmployeeData;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.model.RequestState;

@Service
public class LeaveRequestService {

	@Autowired
	private LeaveRequestDAO dao;
	
	
	public List<LeaveRequest> getAll() {
		return dao.findAll();
	}
	
	public List<LeaveRequest> getAllLeaveRequestsFromOneEmployee(EmployeeData employeeData) {
		return dao.findAllByEmployeeData(employeeData);
	}
	
	public LeaveRequest saveLeaveRequest (LeaveRequest lr) {
		return dao.save(lr);
	}
	
	public boolean deleteLeaveRequest(int id ) {
		if (!dao.existsById(id)) {
			throw new IllegalArgumentException();
		}
		dao.deleteById(id);
		return !dao.existsById(id);
	}
	
	public LeaveRequest updateLeaveRequest(int id, RequestState newState) {
		Optional<LeaveRequest> o = dao.findById(id);
		if(o.isPresent()) {
			LeaveRequest lr = o.get();
			return dao.save(lr);
		}
		return null;
	}
	
}
