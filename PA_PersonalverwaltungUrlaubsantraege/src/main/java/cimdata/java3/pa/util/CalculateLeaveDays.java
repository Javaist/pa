package cimdata.java3.pa.util;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class CalculateLeaveDays {

	private CalculateLeaveDays() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Calculate leaveDays excluding weekend
	 * @return leaveDays
	 */
	public static int calculate(LocalDate leaveFrom, LocalDate leaveTo) {
		final LocalDate start = leaveFrom;
	    final LocalDate end = leaveTo;

	    int leaveDays = 0;
	    LocalDate weekday = start;

	    if (start.getDayOfWeek() == DayOfWeek.SATURDAY ) {
	    	weekday = weekday.plusDays(2);
	    } else if(start.getDayOfWeek() == DayOfWeek.SUNDAY) {
	    	weekday = weekday.plusDays(1);
	    }

	    while (weekday.isBefore(end.plusDays(1))) {
//	        System.out.println(weekday + "__ " + leaveDays);

	        if (weekday.getDayOfWeek() == DayOfWeek.FRIDAY) {
	        	weekday = weekday.plusDays(3);
	        	leaveDays++;
	        } else {
	        	weekday = weekday.plusDays(1);
	        	leaveDays++;
	        }
	    }
	
	    return leaveDays;
//		return leaveDays = (int) ChronoUnit.DAYS.between(leaveFrom, leaveTo);
	}
	
}
