package cimdata.java3.pa.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class OnScreenMessage {

	
	private OnScreenMessage() {
		// TODO Auto-generated constructor stub
	}
	
	public static void addMessage(Severity messageTyp, String title, String detailInfo) {
		FacesMessage message = new FacesMessage(messageTyp, title, detailInfo);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
