-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 26. Mrz 2021 um 08:08
-- Server-Version: 5.7.32
-- PHP-Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `java3_pa`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(30) DEFAULT NULL,
  `postcode` int(5) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `street_number` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `address`
--

TRUNCATE TABLE `address`;
--
-- Daten für Tabelle `address`
--

INSERT INTO `address` (`id`, `city`, `postcode`, `street`, `street_number`) VALUES
(1, 'Berlin', '10559', 'Dorfstraße', '13A'),
(2, 'Köln', '50667', 'Joachimstraße', '32'),
(3, 'Hamburg', '22550', 'Hochstr.', '99'),
(4, 'Schwerin', '19063', 'Am Ring', '134 A'),
(5, 'Mainz', '55131', 'Dortmunder Allee', '2b'),
(6, 'Neubrandenburg', '17034', 'Brodaer Höhe', '56');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorisation` varchar(10) DEFAULT NULL,
  `employee_id` varchar(10) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `employee_data_id` int(11) DEFAULT NULL,
  `login_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_data_id` (`employee_data_id`),
  KEY `login_id` (`login_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `employee`
--

TRUNCATE TABLE `employee`;
--
-- Daten für Tabelle `employee`
--

INSERT INTO `employee` (`id`, `authorisation`, `employee_id`, `firstname`, `lastname`, `employee_data_id`, `login_id`) VALUES
(1, 'USER', 'cim0001', 'Thomas', 'Müller', 1, 1),
(2, 'USER', 'cim0002', 'Regina', 'Schmidt', 2, 2),
(3, 'ADMIN', 'cim0003', 'Ben', 'Javaist', 3, 3),
(4, 'USER', 'cim0004', 'Paula', 'Peters', 4, 4),
(5, 'SUPERVISOR', 'cim0005', 'Sabine', 'Bäcker', 5, 5),
(6, 'USER', 'cim0006', 'Alfred', 'Klingel', 6, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `employee_data`
--

CREATE TABLE IF NOT EXISTS `employee_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_of_hire` date DEFAULT NULL,
  `days_of_illness` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `leave_entitlement_per_annum` int(11) NOT NULL,
  `salary` double DEFAULT NULL,
  `taken_leave` int(11) NOT NULL,
  `temporary_employment` bit(1) NOT NULL,
  `temporary_employment_to` date DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `employee_data`
--

TRUNCATE TABLE `employee_data`;
--
-- Daten für Tabelle `employee_data`
--

INSERT INTO `employee_data` (`id`, `day_of_hire`, `days_of_illness`, `dob`, `email`, `leave_entitlement_per_annum`, `salary`, `taken_leave`, `temporary_employment`, `temporary_employment_to`, `address_id`) VALUES
(1, '2012-11-09', 1, '1990-01-03', 'thomas.mueller@examail.de', 24, 2950.5, 0, b'0', NULL, 1),
(2, '2005-02-12', 0, '1985-05-23', 'r.schmidt@example-cimdata.de', 30, 3582, 1, b'0', NULL, 2),
(3, '2021-01-02', 2, '1981-11-09', 'ben.javaist@example.mail', 24, 3100, 0, b'1', '2021-07-02', 3),
(4, '2010-10-04', 1, '1989-08-09', 'peters@examail.de', 26, 3950.5, 0, b'0', NULL, 4),
(5, '2005-01-31', 0, '1969-04-19', 's.beacker@example-cimdata.de', 27, 3200, 1, b'0', NULL, 5),
(6, '2021-03-01', 2, '1995-12-24', 'a.klingel@example.mail', 24, 2344.75, 0, b'1', '2021-05-03', 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `leave_requests`
--

CREATE TABLE IF NOT EXISTS `leave_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_days` int(11) NOT NULL,
  `submit_date` datetime DEFAULT NULL,
  `leave_from` date DEFAULT NULL,
  `leave_to` date DEFAULT NULL,
  `request_state` varchar(9) DEFAULT NULL,
  `statement` text,
  `employee_data_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_data_id` (`employee_data_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `leave_requests`
--

TRUNCATE TABLE `leave_requests`;
--
-- Daten für Tabelle `leave_requests`
--

INSERT INTO `leave_requests` (`id`, `leave_days`, `submit_date`, `leave_from`, `leave_to`, `request_state`, `statement`, `employee_data_id`) VALUES
(1, 6, '2021-03-25 09:54:34', '2021-05-03', '2021-05-10', 'UNCONFIRMED', NULL, 1),
(2, 4, '2021-03-25 14:22:14', '2021-06-01', '2021-06-05', 'UNCONFIRMED', NULL, 1),
(3, 0, '2021-03-24 18:12:08', '2023-05-03', '2023-05-10', 'UNCONFIRMED', NULL, 3),
(4, 0, '2021-03-25 20:10:01', '2021-07-07', '2021-07-07', 'UNCONFIRMED', NULL, 2),
(5, 1, '2021-03-25 13:55:56', '2021-08-03', '2021-08-03', 'UNCONFIRMED', NULL, 1),
(6, 0, '2021-03-25 08:07:24', '2021-06-01', '2021-06-05', 'UNCONFIRMED', NULL, 4),
(7, 0, '2021-03-25 14:33:43', '2022-01-02', '2021-12-20', 'UNCONFIRMED', NULL, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blocked` bit(1) NOT NULL,
  `incorrect_login_count` int(11) NOT NULL,
  `last_login` date DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- TRUNCATE Tabelle vor dem Einfügen `logins`
--

TRUNCATE TABLE `logins`;
--
-- Daten für Tabelle `logins`
--

INSERT INTO `logins` (`id`, `blocked`, `incorrect_login_count`, `last_login`, `password`, `username`) VALUES
(1, b'0', 0, NULL, 'abc123', 'tmueller'),
(2, b'0', 0, NULL, 'regina85', 'rschmidt'),
(3, b'0', 0, NULL, 'javaMaster', 'bjavaist'),
(4, b'0', 0, NULL, 'p1111', 'ppeters'),
(5, b'0', 0, NULL, 'brot2000', 'sbaecker'),
(6, b'0', 0, NULL, 'javaMaster', 'aklingel');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
