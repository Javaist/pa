package cimdata.java3.pa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cimdata.java3.pa.dao.EmployeeDAO;
import cimdata.java3.pa.dao.LeaveRequestDAO;
import cimdata.java3.pa.model.Address;
import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.EmployeeData;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.model.LoginDetails;
import cimdata.java3.pa.model.UserAuthorisation;

@SpringBootTest
public class LeaveRequestServiceTest {

	@Autowired
	private LeaveRequestService lrService;

	@Autowired
	private EmployeeService empService;

	@Autowired
	private EmployeeDAO empDao;
	
	@Autowired
	private LeaveRequestDAO lrDao;

	@BeforeEach
	void init() {
		empDao.deleteAll(); // clear DB
		
		Employee emp_1 = new Employee("cim0001", "Thomas", "Müller", UserAuthorisation.USER,
				new EmployeeData(LocalDate.of(1990, 1, 3), new Address("Dorfstraße", "13A", 10559, "Berlin"),
						"thomas.mueller@examail.de", LocalDate.of(2012, 11, 9), 2950.50, false, null, 1, 24, 0),
				new LoginDetails("tmueller", "abc123"));
		Employee emp_2 = new Employee("cim0002", "Regina", "Schmidt", UserAuthorisation.USER,
				new EmployeeData(LocalDate.of(1985, 5, 23), new Address("Joachimstraße", "32", 50667, "Köln"),
						"r.schmidt@example-cimdata.de", LocalDate.of(2005, 02, 12), 3582.0, false, null, 0, 30, 1),
				new LoginDetails("rschmidt", "regina85"));
		Employee emp_3 = new Employee("cim0003", "Ben", "Javaist", UserAuthorisation.ADMIN,
				new EmployeeData(LocalDate.of(1981, 11, 9), new Address("Hochstr.", "99", 22550, "Hamburg"),
						"ben.javaist@example.mail", LocalDate.of(2021, 01, 02), 3100.00, true,
						LocalDate.of(2021, 07, 2), 2, 24, 0),
				new LoginDetails("bjavaist", "javaMaster"));

		emp_1.getEmployeeData()
				.addLeaveRequest(new LeaveRequest(LocalDate.of(2021, 05, 03), LocalDate.of(2021, 05, 10)));
		emp_1.getEmployeeData().addLeaveRequest(new LeaveRequest(LocalDate.of(2021, 6, 01), LocalDate.of(2021, 6, 5)));
		emp_3.getEmployeeData()
				.addLeaveRequest(new LeaveRequest(LocalDate.of(2023, 05, 03), LocalDate.of(2023, 05, 10)));

		empService.attachEmployee(emp_1);
		empService.attachEmployee(emp_2);
		empService.attachEmployee(emp_3);
	}

	@AfterEach
	void clear() {
		empDao.deleteAll();
	}

	@Test
	public void testGetAll() {
		List<LeaveRequest> list = lrService.getAll();

		assertNotNull(list);
//		System.out.println("<<<<lr list size: " + list.size());
		assertTrue(list.size() == 3);
	}

//	@Test
//	public void testSaveLeaveRequest() {
//
//		Employee emp = empService.findEmployeeByEmployeeId("cim0002");
//		LeaveRequest newLr = new LeaveRequest(LocalDate.of(2021, 7, 1), LocalDate.of(2021, 7, 7));
//
//		LeaveRequest savedLr = emp.getEmployeeData().addLeaveRequest(newLr);
//
//		assertNotNull(savedLr);
//		assertEquals(newLr, savedLr);
//
//		// erneut Mitarbeiter "cim0002" abrufen; nun sollt ein Urlaubsantrag in der liste sein
//		Employee getEmp2FromDb_2 = empService.findEmployeeByEmployeeId("cim0002");
////		System.out.println(getEmp2FromDb_2.getEmployeeData().getLeaveRequestList().size());
//		assertTrue(getEmp2FromDb_2.getEmployeeData().getLeaveRequestList().size() == 1);
//		assertEquals(LocalDate.of(2021, 7, 1), getEmp2FromDb_2.getEmployeeData().getLeaveRequestList().get(0).getLeaveFrom());
//		assertEquals(50667, getEmp2FromDb_2.getEmployeeData().getLeaveRequestList().get(0).getEmployeeData().getAddress().getPostcode());
//
////		 Die Menge der Informationen/verketteten Objekte überfordern die equals, trotz apache.commons.lang3.builder  
//		assertEquals(getEmp2FromDb_2.getEmployeeData().getLeaveRequestList().get(0), savedLr);
//
//	}

//	@Test
////	@Transactional  // stattdessen eine anpassung im Entity EmployeeData in der Beziehung zu LeaveRequest(List)
//	public void testDeleteLeaveRequest() {
//		
//		Employee emp = empService.findEmployeeByEmployeeId("cim0003");
//		LeaveRequest newLr = new LeaveRequest(LocalDate.of(2021, 8, 1), LocalDate.of(2022, 7, 7));
//		LeaveRequest savedLr = emp.getEmployeeData().addLeaveRequest(newLr);
//		
//		assertNotNull(savedLr);
//		assertEquals(newLr, savedLr);
//		Long id = savedLr.getId();
//		System.out.println("id: " + id);
////		int id = empService.findEmployeeByEmployeeId("cim0003").getEmployeeData().getLeaveRequestList().get(0).getId();
//		boolean deleted = lrService.deleteLeaveRequest(id);
//		assertTrue(deleted);
//		assertFalse(lrDao.existsById(id));
//	}
	
	
	@Test
	public void testGetAllLeaveRequestsFromOneEmployee() {
		
		Employee emp = empService.findEmployeeByEmployeeId("cim0001");
		
		List<LeaveRequest> list = lrService.getAllLeaveRequestsFromOneEmployee(emp.getEmployeeData());
		System.out.println(list.size());
		assertTrue(list.size() == 2);
		
	}
	
}
