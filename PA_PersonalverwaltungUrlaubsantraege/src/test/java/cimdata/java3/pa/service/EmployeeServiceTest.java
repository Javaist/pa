package cimdata.java3.pa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cimdata.java3.pa.dao.EmployeeDAO;
import cimdata.java3.pa.model.Address;
import cimdata.java3.pa.model.Employee;
import cimdata.java3.pa.model.EmployeeData;
import cimdata.java3.pa.model.LeaveRequest;
import cimdata.java3.pa.model.LoginDetails;
import cimdata.java3.pa.model.UserAuthorisation;

@SpringBootTest
public class EmployeeServiceTest {
	
	@Autowired
	private EmployeeService service;
	
	@Autowired
	private EmployeeDAO dao;
	
	
	@BeforeEach
	void init() {
		dao.deleteAll();
		
		service.attachEmployee(new Employee("cim0001", "Thomas", "Müller", UserAuthorisation.USER, new EmployeeData(LocalDate.of(1990, 1, 3), new Address("Dorfstraße", "13A", 10559, "Berlin"), "thomas.mueller@examail.de", LocalDate.of(2012, 11, 9), 2950.50, false, null, 1, 24, 0), new LoginDetails("tmueller", "abc123")));
		service.attachEmployee(new Employee("cim0002", "Regina", "Schmidt", UserAuthorisation.USER, new EmployeeData(LocalDate.of(1985, 5, 23), new Address("Joachimstraße", "32", 50667, "Köln"), "r.schmidt@example-cimdata.de", LocalDate.of(2005, 02, 12), 3582.0, false, null, 0, 30, 1), new LoginDetails("rschmidt", "regina85")));
		service.attachEmployee(new Employee("cim0003", "Ben", "Javaist", UserAuthorisation.ADMIN, new EmployeeData(LocalDate.of(1981, 11, 9), new Address("Hochstr.", "99", 22550, "Hamburg"), "ben.javaist@example.mail", LocalDate.of(2021, 01, 02), 3100.00, true, LocalDate.of(2021, 07, 2), 2, 24, 0), new LoginDetails("bjavaist", "javaMaster")));
	}
	
	@AfterEach
	void clear() {
		dao.deleteAll();
	}
	
	@Test
	public void testGetAllEmployees() {
		List<Employee> list = new ArrayList<Employee>();
		
		list = service.getAll();
		
		assertNotNull(list);
		assertTrue(list.size() == 3);
		
		int i = 0;
		for (Employee employee : list) {
			if(employee.getFirstname().equals("Regina")) {
				i = list.indexOf(employee);
			}
		}
		
		assertEquals("Regina", list.get(i).getFirstname());
		assertEquals("cim0002", list.get(i).getEmployeeId());
		assertEquals(LocalDate.of(1985, 5, 23), list.get(i).getEmployeeData().getDob());
	}
	
	@Test
	public void testAttachEmployee() {
		service.attachEmployee(new Employee("cim0004", "Kurt", "Krömer", UserAuthorisation.USER, new EmployeeData(LocalDate.of(1979, 12, 2), new Address("Alexanderplatz", "1A", 12345, "Berlin"), "kroemer@examail.rbb", LocalDate.of(2020, 11, 9), 2150.25, false, null, 0, 25, 5), new LoginDetails("kkroemer", "Chez79")));

		List<Employee> list = new ArrayList<Employee>();
		
		list = service.getAll();
		
		assertNotNull(list);
		assertTrue(list.size() == 4);
		
		int i = 0;
		for (Employee employee : list) {
			if(employee.getEmployeeId().equals("cim0004")) {
				i = list.indexOf(employee);
			}
		}
		
		assertEquals("cim0004", list.get(i).getEmployeeId());
		assertEquals(2150.25, list.get(i).getEmployeeData().getSalary());
		assertEquals("Chez79", list.get(i).getLogin().getPassword());
	}
	
	@Test
	public void testFindEmployeeByUsername() {
		Employee emp = service.findEmployeeByUsername("rschmidt");
		
//		System.out.println("<<<<<< " + emp.toString());  // produce StringBuilder error
		assertNotNull(emp);
		assertEquals("cim0002", emp.getEmployeeId());
		
	}
	
	
	
}
